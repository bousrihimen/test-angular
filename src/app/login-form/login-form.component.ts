import { LoginServiceService } from './../Service/login-service.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginComponent implements OnInit {
 // loginForm!:FormGroup;
  _hide = true;

  constructor(
    private http: HttpClient,
    private loginService: LoginServiceService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {}


loginForm = this.formBuilder.group({
    _email: ['',  Validators.required,
                  Validators.minLength(6),
                  Validators.email,
                  Validators.pattern(/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,3}$/)
 ],
    _MOT_DE_PASSE: ['', Validators.required, 
                        Validators['minLength'](4)]
 });
 
 hidePassword: boolean = true;

 togglePasswordVisibility() {
   this.hidePassword = !this.hidePassword;
 }
   

  ngOnInit() {
         // ngOnInit(), vous pouvez effectuer d'autres tâches d'initialisation,
      // telles que la récupération de données initiales à partir d'un service
      //  ou la configuration des observables. C'est une bonne approche si vous 
      //  avez besoin d'effectuer des tâches d'initialisation supplémentaires 
      //  avant l'affichage du composantpérer les données initiales et effectuer 
      //  d'autres opérations nécessaires avant l'affichage du composant
 
  }
  

  login(): void {
    const formData = this.loginForm.value;
  
    if(this.loginForm.valid){
     this.http.post(this.loginService.url, formData).subscribe({
      next: (response: any) => {
        console.log('Réponse du backend', response);
        this.router.navigateByUrl('/acceuil');
      },
      error: (error: any) => {
        console.error('Erreur de connexion', error);
      }
    });
   }
  }
}


/*export class LoginComponent implements OnInit {
  loginForm!: FormGroup;

  constructor(
    private http: HttpClient,
    private loginService: LoginServiceService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.loginForm = this.formBuilder.group({
      _email: ['', Validators.required],
      _MOT_DE_PASSE: ['', Validators.required]
    });
  }

  ngOnInit() {
 
  }
  

  login(): Subscription {
    const formData = this.loginForm.value;
    return this.http.post(this.loginService.url, formData).subscribe({
      next: (response: any) => {
        console.log('Réponse du backend', response);
        this.router.navigateByUrl('/acceuil');
      },
      error: (error: any) => {
        console.error('Erreur de connexion', error);
      }
    });
  }
}
 */