
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Utilisateur } from '../models/utilisateur';
import { AbstractControl, FormControl } from '@angular/forms';
@Injectable({providedIn: 'root'} )
export class  LoginServiceService {

  // addControl(controlName: String, control:AbstractControl): void{
  //   this.inscription.addControl(controlName, control);
  // }
 
   private apiUrl = 'http://localhost:8080/api/app';
   url = `${this.apiUrl}/login`;
   urlInscript = `${this.apiUrl}/inscription`;
  
constructor( private http:HttpClient){}
  login( username: String , password: String): Observable<any>{
   
    const body = {"_email":username,"_MOT_DE_PASSE": password};
    return this.http.post(this.url, body);
  }

  inscription(utilisateurData: Utilisateur): Observable<any>{
    const utilisaeurModel = Object.assign({}, utilisateurData);
      return this.http.post(this.urlInscript, utilisaeurModel);

  };
}

