import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login-form/login-form.component';
import { AccueilComponent } from './accueil/accueil.component';
import { InscriptionComponent } from './inscription/inscription.component';

const routes: Routes = [
  {path:'login-form', component:LoginComponent},
  {path: 'acceuil',component:AccueilComponent },
  {path:'inscription', component:InscriptionComponent},
  {path: '',redirectTo:'/login-form', pathMatch:'full'}

];
// component:LoginFormComponent nom class login-form.component.ts
//login-form : nom de dossier pour path
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

