import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import{Router} from '@angular/router';
import { LoginServiceService } from '../Service/login-service.service';
@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {
  inscriptionForm!: FormGroup;
  
  constructor( 
    private http : HttpClient,
    private router:Router,
    private fb: FormBuilder,
    private loginService: LoginServiceService,
    private inscriptService: LoginServiceService
     ){
      this.inscriptionForm = this.fb.group({
        _nom: ['', Validators.required, 
                   Validators['minLength'](3)],
        _prenom: ['', Validators.required, 
                      Validators['minLength'](3)],
        _telephone:['',Validators.required],
        _adresse: ['', Validators.required],
        _email: ['',  Validators.required,
                      Validators.minLength(6),
                      Validators.email,
                      Validators.pattern(/^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,3}$/)],
        _MOT_DE_PASSE: ['', Validators.required,
                            Validators.minLength(6), 
                            Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]+$/)]
      });
     }

     ngOnInit() {
     
    }

  // ngOnInit() {
  //   const utilisateurModel = {
  //     _nom: '',
  //     _prenom: '',
  //     _telephone: '',
  //     _adresse: '',
  //     _email: '',
  //     _MOT_DE_PASSE: ''
  //   };
  //   this.inscriptionForm= this.fb.group({});

  //   Object.keys(utilisateurModel).forEach(key =>{
  //     this.inscriptService.patchValue( this.utilisateur('', Validators.required))
  //   })
  // }

  inscription(): void {
     const formData = this.inscriptionForm.value;
     if(this.inscriptionForm.valid){
    this.inscriptService.inscription(formData).subscribe({
      next: (response: any) => {
        console.log('Response du backend OK', response);
        this.router.navigateByUrl('/login-form');
      },
      error: (error: any) => {
        console.error('Erreur de connexion', error);
      }
    });
  }
 
  }

  get  password() {
    return  this.inscriptionForm.get('_MOT_DE_PASSE');
  }
}

/* inscription(): Subscriber {
     const formData = this.inscriptionForm.value;
    this.inscriptService.inscription(formData).subscribe({
      next: (response: any) => {
        console.log('Response du backend OK', response);
        this.router.navigateByUrl('/login-form');
      },
      error: (error: any) => {
        console.error('Erreur de connexion', error);
      }
      // ,
      // complete:()=>{} sjdb va bloquer   commit tchequepoint
      // provader lien parentenfan output
    });
     
   
  }
}
// Subscriber à l'ecoter
// OBSERVABLE IL EST ECOTER


*/ 

